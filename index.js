import { renderGraph } from './lib/render-graph.js'

const SAMPLE_DATA = 'Jan Mark\nBob Mark\nJan Mary\nBob Mary\nHarry Lucy'

window.addEventListener('load', async () => {

  // Load sample data and generate sample chart
  const dataContainer = document.querySelector(`.data`)
  if (dataContainer) {
    dataContainer.value = SAMPLE_DATA
    renderGraph()
  }

  // Wire up event handlers
  const buttonApply = document.querySelector('.button-apply')
  if (buttonApply) {
    buttonApply.addEventListener('click', () => renderGraph())
  }
})

