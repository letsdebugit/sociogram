export async function fetchGraphData (dataUrl) {
  const response = await fetch(dataUrl)
  if (response.ok) {
    const data = await response.json()
    return data
  }
}
