import { parseGraphData } from './parse-graph-data.js'
import { createGraph } from './create-graph.js'

/**
 * Parses the input data and renders the chart
 */
export function renderGraph () {
  const graphContainer = document.querySelector(`.graph`)
  const dataContainer = document.querySelector(`.data`)
  const errorsContainer = document.querySelector(`.errors`)

  if (dataContainer && graphContainer && errorsContainer) {
    errorsContainer.innerHTML = ''
    const { nodes, links, errors } = parseGraphData(dataContainer.value)
    const hasErrors = errors.length > 0

    if (nodes && links && !hasErrors) {
      createGraph({ nodes, links }, graphContainer)
    }

    if (hasErrors) {
      errorsContainer.style.display = 'flex'
      errorsContainer.innerHTML = errors
        .map(({ line, message }) => line
          ? `Error in line #${line}: ${message}`
          : `Error: ${message}`
        )
        .join('<br>')
    } else {
      errorsContainer.style.display = 'none'
    }
  }
}

