import { ForceGraph } from './graph.js'

let graphElement

export async function createGraph (data, container) {
  if (!data) throw new Error('Graph data is required')
  if (!container) throw new Error('Graph container element is required')

  // Dispose of the previous graph
  if (graphElement) {
    container.removeChild(graphElement)
  }

  const width = container.clientWidth
  const height = container.clientHeight

  graphElement = ForceGraph(data, {
    nodeId: d => d.id,
    nodeGroup: d => d.group,
    nodeTitle: d => `${d.id}\n${d.group}`,
    linkStrokeWidth: l => Math.sqrt(l.value),
    width,
    height
  })

  container.appendChild(graphElement)
}


