/**
 * Converts a series of lines describing graph relationships,
 * formatted as source->target, into graph data
 * @param {String} text
 * @returns Graph data
 */
export function parseGraphData (text) {
  if (text) {
    const errors = []

    // Parse relationship lines into pairs { source, target } representing graph relationships
    const pairs = text
      .split('\n')
      .map(line => line.trim())
      .map(line => line.split(' '))
      .map(([source, target]) => ({
        source: (source || '').trim(),
        target: (target || '').trim()
      }))

    // Extract unique nodes from relationships
    const nodes = []
    let group = 1
    for (const { source, target } of pairs) {
      if (source && !nodes.find(n => n.id === source)) {
        nodes.push({ id: source, group })
        group++
      }
      if (target && !nodes.find(n => n.id === target)) {
        nodes.push({ id: target, group })
        group++
      }
    }

    // Build and validate unique links from relationships
    const links = pairs
      .filter((link, line) => {
        if (!link.source) {
          errors.push({ line, message: `Source node not specified` })
          return false
        }
        if (!link.target) {
          errors.push({ line, message: `Target node not specified` })
          return false
        }
        return true
      })
      .map(({ source, target }) => ({ source, target, value: 1 }))

    return { nodes, links, errors }
  }
}
